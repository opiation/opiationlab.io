# A type-safe localizer

This chapter details the challenge and solution to providing type-safe localized messages without errors for missing messages.