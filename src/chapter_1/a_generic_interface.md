# A generic interface

What will our localizer service look like?  The interface that seems most convenient is exchanging a `key` and maybe some `arguments` for a localized message.  We use _keys_ here because we want each key to refer uniquely to **1** message.

### Basic interface

```typescript
let v = 32
type MessageKey = string
type Message = string

interface Localizer {
  localize(key: MessageKey): Message
}
```

### Type-safe keys

```typescript
// data looks like
const messages = {
  greeting: 'Hello'
}

type MessageKey = keyof typeof messages
type Message = string

interface Localizer {
  localize(key: MessageKey): Message
}
```

### Type-safe keys with an extensible agnostic interface

```typescript
// data looks like
const messages = {
  greeting: 'Hello'
}

type AnyMessageKey = string
type AnyMessage = string
type AnyMessages = Record<AnyMessageMey, AnyMessage>

interface Localizer<Messages extends AnyMessages = AnyMessages> {
  localize(key: keyof Messages): Messages[keyof Messages]
}

function createLocalizer<Messages extends AnyMessages>(
  messages: Messages
): Localizer<Messages> {
  return {
    localize(key) {
      const message = messages[key]

      if (typeof message !== 'string') {
        throw new Error(`No message for key ${key}`)
      }

      return messages[key]
    }
  }
}
```

### Type-safe keys with interpolated messages

```typescript
const messages = {
  greeting: (name: string) => `Hello, ${name}!`
}

type AnyMessageKey = string
type AnyMessage = string
type AnyMessageFn = (...args: any[]) => AnyMessage
type AnyMessages = Record<AnyMessageKey, AnyMessageFn>

interface Localizer<Messages extends AnyMessages = AnyMessages> {
  localize(
    key: keyof Messages,
    ...args: Parameters<Messages[keyof Messages]>
  ): string
}

function createLocalizer<Messages extends AnyMessages>(
  messages: Messages
): Localizer<Messages> {
  return {
    localize(key, ...args) {
      const messageFn = messages[key]

      if (typeof messageFn !== 'function') {
        throw new Error(`No message for key ${key}`)
      }

      const message = messageFn(...args)

      if (typeof message !== 'string') {
        throw new Error(`Could not localize message ${key}`)
      }

      return message
    }
  }
}
```

```typescript
interface Localizer<Messages extends AnyMessages = AnyMessages> {
  localize<Key extends keyof Messages>(
    key: Key,
    ...args: Parameters<Messages[Key]>
  ): string
}
```