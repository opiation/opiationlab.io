# Summary

- [Chapter 1 - A Type-safe Localizer](./chapter_1/README.md)
  - [A generic interface](./chapter_1/a_generic_interface.md)
- [Generics](./generics.md)