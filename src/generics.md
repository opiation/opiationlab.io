# Generics

This piece aims to answer the following questions about generics in TypeScript in order to make effective, maintainable choices:

- When _should_ one use generics?
- What _should_ be made generic?

## When _should_ one use generics?

I'll admit this is perhaps a misleading question.  I would prefer to frame the question, _what are the more or less effective ways of using generics?_

**Example 1**

In our first example, we require a function that determines if a given collection contains any elements or not.  For the sake of this exercise, we'll assume _collections_ are lists of text or values whose count is available by `length`.

```typescript
const characters = 'aJdNwiN298Gb4D'
const name = ''
const ids = [42, 19, 4]
const persons = []
```

**Implementation 1**

```typescript
function hasElements(collection: string | Array<any>): boolean {
  return collection.length > 0
}

hasElements(characters) // true
hasElements(name) // false
hasElements(ids) // true
hasElements(persons) // false
```

**Implementation 2 - Using generics**

```typescript
function hasElements<T extends string | Array<any>>(collection: T): boolean {
  return collection.length > 0
}

hasElements(characters) // true
hasElements(name) // false
hasElements(ids) // true
hasElements(persons) // false
```

Both predicates above (`hasElements` and `hasElems`) offer the same functionality and same type safety in most practical use.  In the `hasElems` case however, the type information of the provided argument is preserved throughout the body of the function so why choose the `hasElements` when `hasElems` retains more information?  It is unused information.  The body of the function only makes use of the `string | Array<any>` interface and function does not return any type information that `T` provides over `string | Array<any>`.  Thus, `T extends string | Array<any>` is **unnecessary** complexity and `string | Array<any>` can be used instead (as in `hasElements`).

## What _should_ be made generic?

**Example 2**

When using nested generics, it may not be immediately obvious which type should be made generic and how that type information should be passed on.

```typescript
type Entity = { id: string }
type Role = string

/**
 * Expresses a relationship between a user and a given role in an application
 */
type Actor<User extends Entity = Entity> = {
  user: User
  role: Role
}
```

In the above example, we're developing an authorization library where _actors_ are representations of a user _acting as_ a given role.  Since this library is intended to be consumed by an application with its own notion of what a _user_ is, we use a generic `User` for its `user` type and expect it to at least be any uniquely identifiable entity.  This can be very beneficial as it allows us to keep this authorization model very loosely coupled with our consumer application's notion of _user_.

Suppose however that we need some function that accepts some actor, triggers a side-effect and returns that actor.  Ideally, we want this function to preserve the actor type information because it needs to pass it along so that consumers of the function's return value have all the same type information.  Generics are perfect for this.

```typescript
function notifyActor(actor: Actor<Entity>): Actor<Entity> {
  // perform some side-effect base on the actor's role
  sendNotificationTo(actor.user.id, { role: actor.role })
  return actor
}

type Person = { name: string, birthdate: Date }
const developer: Actor<Person> = {
  user: { name: 'Grace Hopper', birthdate: new Date() },
  role: 'developer'
}

const output = notifyActor(developer)
//    ^ output.user type here is `Entity` even though we know it's a `Person`
//      from the `developer` we provided so using `output.user.birthdate` would
//      appear to be an error in compilation while still actually working :(
console.log(`Developer's birthday: ${output.birthdate}`)
//          ^ Results in compilation error because an `Entity` does not have a
//            `birthdate`

function notifyActor<User extends Entity>(actor: Actor<User>): Actor<User> {
  sendNotificationTo(actor.user.id, { role: actor.role })
  return actor
}
console.log(`Developer's birthday: ${output.birthdate}`)
//          ^ Works as expected!

const output = notifyActor(developer)
//    ^ output.user is `Person` :)
//      We can safely trust our type checker again to reveal actual errors!

// We could howver move the generic type argument "up 1 level" so to speak
// to make the entire argument generic
function notifyActor<A extends Actor<Entity>>(actor: A): A {
  sendNotificationTo(actor.user.id, { role: actor.role })
}
console.log(`Developer's birthday: ${output.birthdate}`)
//           ^ Also works as expected!
```

So... what is the _better_ choice here between the second and third implementations of `notifyActor`?  They both address the false positive compilation error from the first implementation and both preserve the required type information used by the subsequent `console.log`.  What trade-offs are we making between the two which might inform our decision making?  Referring back to the `hasElements` example does not really help us here.

We can ask ourselves the following:
* Does the function body _use_ any of the type information provided by the generic type argument?
  No.  `notifyActor` does not use or care about what type the actor's `user` is, only that is an entity with an `id` which is guaranteed by the `Actor` type anyway.
* Does the return value of the function require any of the type information provided by the generic type argument?
  Yes.  The subsequent `console.log` statement expects the output's `user` to have a `birthdate` which is only available if the provided actor's `user` is a `Person`.

The key discriminating factor here is that `notifyActor` does not need to know about the specific type of the actor's `user`, only that it is an `Entity`.  Thus, the type of specific type of `user` is outside the scope of this function's responsibility.  Making the `user` generic (as in the second implementation) is effectively requiring the `notifyActor` to know more about the internals of the `actor` than is required.

## Example case

Now, let's draft an implementation for an example case and see if we can use some of the options we've learned about previously.  In our example case, we'll need to implement a service that publishes articles and uses our own authorization model to determine whether a given user is authorized or not authorized to do so.  As we expect to need such an authorization model across other services of our platform, we'll need to implement such that it can be used _easily enough_ with other services.

For the initial requirements of this endeavor, we need to express a relationship that a user can have with an article.  We'll say that a user that _authors_ an article acts as an **author** of that article, a user that _reviews_ an atricle acts as a **reviewer** of that article, and a user that publishes an article acts as a **publisher** of that article.  We'll call those _roles_ as they represent the various roles a user may inhabit with respect to an article.  For lack of a better name, we'll call this relationship between a user, article and role an **actor**.

Before we get too carried away in the domain of identity and access management, let's define some types to look roughly like what we need based on the things we just talked about.

```typescript
type Role = 'author' | 'publisher' | 'reviewer'

type User = {
  id: ID
  email: string
}

type Article = {
  id: ID
  title: string
  content: string
}

type Actor = {
  article: Article
  user: User
  role: Role
}
```

> `ID` is not defined above but means _some attribute that uniquely identifies a record of this type_.  In practice, this is most often a database ID but does not need to be.  For the sake of these examples, we can implement the type as `type ID = string`.

In the above draft, we've defined the core _things_ we discussed earlier and some measure of how they are expected to interact with one another.  At the moment, all types reside together which allows them to freely reference one another without concern for responsibility boundaries.  We will revisit this later.  For now, let's implement a function that allows a given requester to publish a given article or throws an error if the requester is not authorized to do so.

```typescript
// Fetches actors relating to a given article
declare function actorsFor(article: Article): Actor[];

// Sends a given article to a hosting service, making it available on the WWW
declare function sendToHostingService(article: Article): void;

/**
 * @param requester user requesting that an article be published
 * @param article to be published
 */
function publishArticle(requester: User, article: Article): void {
  const articleActors = actorsFor(article)

  const publisherActor = articleActors.find(
    actor => actor.user === requester && actor.role === 'publisher'
  )

  if (publisherActor) {
    sendToHostingService(article)
  } else {
    throw new Error(`${requester.email} is not authorized to publish ${article.title}`)
  }
}
```

> `actorsFor` and `sendToHostingService` were stubbed here as they are not central to the topics of interest.  In practice, they would probably be asynchronous requests to different backends, making `publishArticle` asynchronous as well.

> I would not normally throw an exception to signal an authorization failure but rather return an _Either_ with an error message.  I've thrown an exception here to keep function signatures simple and focus on the task at hand.

So, there we have it.  Someone may request that a given article be published.  If this were part of a web interface, one might imagine this function used when the user clicks on a **Publish** button where the `requester` is the currently signed-in user and `article` is the article they're currently viewing.  One might also use IDs instead of the records themselves but we're keeping this simple.

### Enter new requirements

Our authorization model is required for a community organization service that ties into the platform.  We'd like to use all the same notions of actors with roles but the user and records of this community organization service differ.  Currently, the **actor** we've defined accepts users (from the article sharing service) and articles.  Let's generalize this notion of an actor now such that it can cater to the needs of this social media service.

Ideally, we can extract the actor/role component out of the article service such that it can be used by the community organization service:

```mermaid
graph TD

authorization(IAM<br />Actors and Roles)
article_sharing(Article Share<br />Users and Articles)

authorization --> article_sharing
```

```mermaid
graph TD

authorization(IAM<br />Actors and Roles)
article_sharing(Article Share<br />Users and Articles)
community_org(Community Org<br />Users and Projects)

authorization --> article_sharing
authorization --> community_org
```

```typescript
// Article sharing component

type Article = {
  id: ID
  title: string
  content: string
}

type User = {
  id: ID
  email: string
}


// Community organizing component

type Project = {
  id: ID
  title: string
}

type Member = {
  id: ID
  email: string
}


// Authorization library

type Actor<E extends { id: ID }, U extends { id: ID }> = {
  entity: E
  user: U
  role: string
}
```